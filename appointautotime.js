var body = $response.body;
var obj = JSON.parse(body);

let now = new Date();
let year = now.getFullYear(); // 获取当前年份
let month = now.getMonth() + 1; // 获取当前月份（注意月份从0开始，所以需要+1）
let day = now.getDate(); // 获取当前日

month = month < 10 ? "0" + month : month;
day = day < 10 ? "0" + day : day;
var dateFormat = year + "-" + month + "-" + day;
obj["result"]["appiont_date"] = dateFormat;

let hours = now.getHours(); // 获取当前小时
let minutes = now.getMinutes(); // 获取当前分钟
let timeMin = 17 * 60 + 30; // 将17:30转化为分钟
let timeMax = 18 * 60 + 30; // 将18:30转化为分钟

let isWithinTimeRange =
  hours * 60 + minutes >= timeMin && hours * 60 + minutes <= timeMax;
if (isWithinTimeRange) {
  var starttime = "17:40";
  var endtime = "17:50";
  if (minutes >= 30 && minutes < 40) {
    starttime = hours + ":30";
    endtime = hours + ":40";
  } else if (minutes >= 20 && minutes < 30) {
    starttime = hours + ":20";
    endtime = hours + ":30";
  } else if (minutes >= 10 && minutes < 20) {
    starttime = hours + ":10";
    endtime = hours + ":20";
  } else if (minutes >= 0 && minutes < 10) {
    starttime = hours + ":00";
    endtime = hours + ":10";
  } else if (minutes >= 50 && minutes < 60) {
    starttime = "17:50";
    endtime = "18:00";
  }
  obj["result"]["start_time"] = starttime;
  obj["result"]["end_time"] = endtime;
}

body = JSON.stringify(obj);
$done(body);
